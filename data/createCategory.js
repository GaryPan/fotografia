var Kaiseki = require('kaiseki');

var APP_ID = 'l8YIQiM8vKxOqGcOyTqcAYBkGPIWvTKnX9KevvOi',
	REST_API_KEY = 'i22UtKYOfDEOEUkyWOkxufBmaTcutlll3xMU7qZ5',
	kaiseki = new Kaiseki(APP_ID, REST_API_KEY),
	className = 'CategoryObject',
	categoryObject = 
 	[{
		catId: 0,
		name: "Night",
		groups: [
			{
				groupId: "11947580@N00",
				groupUrl: "http://www.flickr.com/groups/11947580@N00",
				pool_count: 0
			},
			{
				groupId: "83812269@N00",
				groupUrl: 'http://www.flickr.com/groups/nightlights',
				pool_count: 0
			}
		]
	},
	{
		catId: 1,
		name: "People Portaits",
		groups: [
			{
				groupId: "52240944201@N01",
				groupUrl: "http://www.flickr.com/groups/people",
				pool_count: 0

			}
		]
	},
	{
		catId: 2,
		name: "Landscape",
		groups: [
			{
				groupId: "13495780@N00",
				groupUrl: "http://www.flickr.com/groups/landscape",
				pool_count: 0

			}
		]
	},
	{
		catId: 3,
		name: "Black and White",
		groups: [
			{
				groupId: "16978849@N00",
				groupUrl: "http://www.flickr.com/groups/blackandwhite",
				pool_count: 0

			},
			{
				groupId: "52239733174@N01",
				groupUrl: "http://www.flickr.com/groups/blackwhite",
				pool_count: 0

			},
			{
				groupId: "61859776@N00",
				groupUrl: "http://www.flickr.com/groups/black_and_white_4_beginners",
				pool_count: 0

			}
		]
	},
	{
		catId: 4,
		name: "Sunsets & Sunrises",
		groups: [
			{
				groupId: "52242317293@N01",
				groupUrl: "http://www.flickr.com/groups/sun",
				pool_count: 0

			},
			{
				groupId: "52239829790@N01",
				groupUrl: "http://www.flickr.com/groups/sunrisesunset",
				pool_count: 0

			}
		]
	},
	{
		catId: 5,
		name: "Architecture",
		groups: [
			{
				groupId: "39804613888@N01",
				groupUrl: "http://www.flickr.com/groups/architecture",
				pool_count: 0

			}
		]
	},
	{
		catId: 6,
		name: "Sky & Clouds",
		groups: [
			{
				groupId: "89594630@N00",
				groupUrl: "http://www.flickr.com/groups/skyandclouds",
				pool_count: 0

			}
		]
	}];

var loopLength = categoryObject.length,
	i = 0;

for (i = 0; i < loopLength; i++) {
	kaiseki.createObject(className, categoryObject[i], function(err, res, body, success) {
		console.log('object created = ', body);
		console.log('object id = ', body.objectId);
	});
}

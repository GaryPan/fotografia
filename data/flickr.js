var http = require('http');
var Q = require("q");

function api_flickr(options) {
	this._host = 'api.flickr.com';
	this._path = '/services/rest/?format=json&nojsoncallback=1';
	this._api_key = options.api_key || '';
	this._api_secret = options.api_secret || '';
};

api_flickr.prototype = {
	getBasePath: function() {
		return this._path;
	},
	getHost: function() {
		return this._host;
	},
	getApiKey: function() {
		return this._api_key;
	},
	getApiSecret: function() {
		return this._api_secret;
	},
	_updateGroup: function() {

	},
	_RESTCall: function(path) {
		var deferred = Q.defer();
		var options = {
			host: this.getHost(),
			path: path
		};
		
		var data = '';
		http.get(options, function(resp) {
			resp.on('data', function(chunk) {
				data += chunk;
			});
			resp.on('end', function() {
				// console.log('resp data');
				var json = JSON.parse(data);
				// console.log(json);
				deferred.resolve(json);
			});
		}).on("error", function(error) {
			console.log(error);
			deferred.reject(new Error(error));
		});
		return deferred.promise;
	},
	callAPI: function(options) {
		// console.log('call api');
		// console.log(options);
		var deferred = Q.defer();
		var path = this.getBasePath() + '&method=' + options.method + '&api_key=' + this.getApiKey();

		if(options.per_page) {
			path += '&per_page=' + options.per_page;
		}
		if(options.page) {
			path += '&page=' + options.page;
		}
		if(options.sort){
			path += '&sort=' + options.sort;
		}
		var newPath;
		switch(options.method) {
			case 'flickr.urls.lookupGroup':
				if(options.url) {
					newPath = path + '&url=' + options.url;
				}
				break;
			case 'flickr.groups.pools.getPhotos':
				if(options.group_id) {
					newPath = path + '&group_id=' + options.group_id;
				}
				break;
			case 'flickr.photos.getSizes':
				if(options.photo_id){
					newPath = path + '&photo_id=' + options.photo_id;
				}
				break;
			case 'flickr.photos.getExif':
				if(options.photo_id){
					newPath = path + '&photo_id=' + options.photo_id;
				}
				// console.log(this.getHost()+newPath);
				break;
			// case 'flickr.photos.search':
			// 	if(options.text) {
			// 		options.text.forEach(function(keyword){
			// 			var newPath = path + '&text=' + keyword;
			// 			this._search(newPath);
			// 		}.bind(this));
			// 	}
			// 	if(options.tags) {
			// 		options.tags.forEach(function(tag) {
			// 			var newPath = path + '&tags=' + tag;
			// 			this._search(newPath);
			// 		}.bind(this));
			// 	}
			// 	break;
			// case 'flickr.photos.getInfo': 
			// 	if(options.photo_id){
			// 		var newPath = path + '&photo_id=' + options.photo_id;
			// 		this._getInfo(newPath, options.photo_id);
			// 	}
			// 	break;
		}
		// console.log('newPath');
		// console.log(newPath);
		this._RESTCall(newPath).then(function (resp) {
			deferred.resolve(resp);
		});
		return deferred.promise;
	}
};

module.exports = api_flickr;
var _api_flickr = require('./flickr.js');
var Kaiseki = require('kaiseki');

var APP_ID = 'l8YIQiM8vKxOqGcOyTqcAYBkGPIWvTKnX9KevvOi',
	REST_API_KEY = 'i22UtKYOfDEOEUkyWOkxufBmaTcutlll3xMU7qZ5',
	kaiseki = new Kaiseki(APP_ID, REST_API_KEY);

var flickr = new _api_flickr({
	api_key: '98e8d3e9280f95476ce3139288c41638',
	api_secret: '14a2638159c6effe'
});
    
// flickr.callAPI({
// 	method: 'flickr.urls.lookupGroup',
// 	url: 'http://www.flickr.com/groups/nightlights'
// }).then(function (resp) {
// 	console.log(resp);
// 	return resp;
// });
var getGroupPhotos = function () {
	var params = {
		where: { },
		order: 'catId'
	};
	kaiseki.getObject('CategoryObject', '', params, function(err, res, category, success) {
		var i = 0;
			categoryLength = category.length;
		for (i = 0; i < categoryLength; i++) {
			var j = 0,
				objectId = category[i].objectId;

			console.log(category[i].name);
			//console.log(category[i].groups);
			
			for (j = 0; j < category[i].groups.length; j++) {
				// console.log(category[i].groups[j]);

				getPhotosByPage(category[i].groups[j].groupId, category[i].catId, 1);
				
			}
		}
	});
};

var getPhotosByPage = function(groupId, categoryId, page) {
	flickr.callAPI({
		method: 'flickr.groups.pools.getPhotos',
		group_id: groupId,
		per_page: 100,
		page: page
	}).then(function (photos) {
		var totalPages = photos.photos.pages,
			y = 0;

		for(y=0; y<photos.photos.photo.length; y++) {
			var photoId = photos.photos.photo[y].id;
			// console.log('photo id');
			// console.log(photoId);

			// filter size
			filterPhoto(groupId, categoryId, photoId, page);

		}
		var nextPage = page+1;
		// if(page+1 <= totalPages) {
		if(nextPage <= 20) {
			console.log('total pages: '+ totalPages + ', go page: '+ nextPage);
			setTimeout(getPhotosByPage(groupId, categoryId, nextPage), 100);
			// getPhotosByPage(groupId, categoryId, nextPage);
		}
	});
};

var filterPhoto = function(groupId, categoryId, photoId, page) {
	flickr.callAPI({
		method: 'flickr.photos.getSizes',
		photo_id: photoId
	}).then(function (size) {
		// console.log('size');
		// console.log(size);
		var lastSizeLength = size.sizes.size.length -1;
		
		if (parseInt(size.sizes.size[lastSizeLength].width) >= 1024) {
			// console.log('photo id: ' + photoId + ', size: ' + size.sizes.size[lastSizeLength].width);
			// filter exif
			flickr.callAPI({
				method: 'flickr.photos.getExif',
				photo_id: photoId
			}).then(function (exif) {
				var x = 0,
					exifLength = exif.photo.exif.length,
					save = false,
					photoObject = {
						catId: categoryId,
						photoId: exif.photo.id,
						squareSource: size.sizes.size[0].source,
						originalSource: size.sizes.size[lastSizeLength].source,
						make: '', //Make, 
						model: '',  //Model
						exposureTime: '', //ExposureTime, 
						exposureTime_clean: '', 
						fNumber: '', //FNumber
						fNumber_clean: '', 
						ISO: '',	//ISO
						exposureCompensation: '',	//ExposureCompensation
						exposureCompensation_clean: '',
						flash: '', //Flash
						focalLength: '', //FocalLength
						focalLength_clean: ''
					};
					//raw._content, clean._content
				// console.log(exif.photo.id +' exifLength' + exifLength);

				for(x=0; x<exifLength; x++) {
					// console.log('x' + x + ' ,' + exif.photo.exif[x].tag);
					switch(exif.photo.exif[x].tag) {
						case 'Make':
							photoObject.make = exif.photo.exif[x].raw._content;
							break;
						case 'Model':
							photoObject.model = exif.photo.exif[x].raw._content;
							break;
						case 'ExposureTime':
						// console.log('!!!!!GOT IT!!!!!');
							save = true;
							photoObject.exposureTime = exif.photo.exif[x].raw._content;
							photoObject.exposureTime_clean = exif.photo.exif[x].clean._content;
							break;
						case 'FNumber':
							photoObject.fNumber = exif.photo.exif[x].raw._content;
							photoObject.fNumber_clean = exif.photo.exif[x].clean._content;
							break;
						case 'ISO':
							photoObject.ISO = exif.photo.exif[x].raw._content;
							break;
						case 'ExposureCompensation':
							photoObject.exposureCompensation = exif.photo.exif[x].raw._content;
							photoObject.exposureCompensation_clean = exif.photo.exif[x].clean._content;
							break;
						case 'Flash':
							photoObject.flash = exif.photo.exif[x].raw._content;
							break;
						case 'FocalLength':
							photoObject.focalLength = exif.photo.exif[x].raw._content;
							photoObject.focalLength_clean = exif.photo.exif[x].clean._content;
							break;
					}
				}

				if(save) {
					// add to parse
					// console.log('photoObject');
					// console.log(photoObject);
					kaiseki.createObject('PhotoObject', photoObject, function(err, res, body, success) {
						console.log('object created = ', body);
						console.log('object id = ', body.objectId);
					});
				}
			});
		}
	});
};

// var updateGroupInfo = function(objectId, updateContent) {
// 	kaiseki.updateObject('CategoryObject', objectId, updateContent,
// 	function(err, res, body, success) {
// 		console.log('object updated at = ', body.updatedAt);
// 	});
// };
getGroupPhotos();

angular.module('fotoApp', 
	[
		'utilities',
		'entry',
		'wall',
		'learn'
	])
	.config(['$routeProvider', '$locationProvider', '$httpProvider', 'FlickrProvider', 
		function($routeProvider, $locationProvider, $httpProvider, FlickrProvider) {
		delete $httpProvider.defaults.headers.common['X-Requested-With'];
		$locationProvider.html5Mode(false);
		$locationProvider.hashPrefix('!');
		FlickrProvider.setApiKey('98e8d3e9280f95476ce3139288c41638');
		$routeProvider
			.when('/', {
				templateUrl: 'views/entry.html', 
				controller: 'entryCtrl',
				resolve: {
					
				}
			})
			.when('/wall/:categoryId', {
				templateUrl: 'views/wall.html', 
				controller: 'wallCtrl',
				resolve: {
					
				}
			})
			.when('/learn/:photoId', {
				templateUrl: 'views/learn.html', 
				controller: 'learnCtrl',
				resolve: {
					
				}
			})
			.otherwise({
				redirectTo: '/'
			});
	}])
	.run(function($rootScope, $location, $templateCache) {
		$rootScope.$on('$viewContentLoaded', function() {
			$templateCache.removeAll();
		});
		$rootScope.catId = 0;
	})
	.controller('appCtrl', function($scope) {
		//
		//alert(window.innerWidth+', '+window.innerHeight);
	});

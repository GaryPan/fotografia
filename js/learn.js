angular.module('learn', 
	[
		'utilities'
	])
	.controller('learnCtrl', ['$scope', '$rootScope', '$routeParams', 'Flickr', 'Parse', 
		function($scope, $rootScope, $routeParams, Flickr, Parse) {
		$scope.catId = $rootScope.catId;
		var photoId = $routeParams.photoId,
			titleUrlStr = '';
		switch(parseInt($scope.catId)) {
			case 0:
				titleUrlStr = 'title_night.png';
				break;
			case 1:
				titleUrlStr = 'title_people.png';
				break;
			case 2:
				titleUrlStr = 'title_landscape.png';
				break;
		}
		$scope.titleUrl = titleUrlStr;

		Flickr.photosGetSizes('flickr.photos.getSizes', photoId).then(function(data) {
			var result = data[8];
			$scope.mainPhoto = result.source;
		});
		Flickr.photosGetInfo('flickr.photos.getInfo', photoId).then(function(data) {
			var result = data.photo;
			console.log(result);
			console.log(result.title._content);
			console.log(result.owner.realname);
			console.log(result.owner.username);

			console.log('http://www.flickr.com/photos/'+result.owner.path_alias);
			console.log(result.urls.url[0]._content);
			console.log(result.license);

			console.log(result.owner.iconfarm);
			console.log(result.owner.iconserver);
			console.log(result.owner.nsid);
			//http://farm{icon-farm}.staticflickr.com/{icon-server}/buddyicons/{nsid}.jpg
			//http://www.flickr.com/images/buddyicon.gif

			//
			$scope.photoTitle = result.title._content;
			$scope.userAvatar = 'http://farm'+result.owner.iconfarm+'.staticflickr.com/'+result.owner.iconserver+'/buddyicons/'+result.owner.nsid+'.jpg';
			$scope.userUrl = 'http://www.flickr.com/photos/'+result.owner.path_alias;
			$scope.userName = result.owner.realname;
			$scope.nickName = result.owner.username;
		});
		var filter = {
			photoId: photoId
		};

		Parse.getClass('PhotoObject', filter).then(function(data) {
			var result = data.results[0];
			console.log(result);
			console.log(result.make+' '+result.model);
			console.log(result.exposureTime);
			console.log(result.fNumber_clean);
			console.log(result.ISO);
			console.log(result.focalLength);
			console.log(result.exposureCompensation_clean);
			console.log(result.flash);

			$scope.cameraModel = result.make+' '+result.model;
			$scope.cameraFocal = result.focalLength;
			$scope.cameraShutter = result.exposureTime;
			$scope.cameraEV = result.exposureCompensation_clean;
			$scope.cameraAperture = result.fNumber_clean;
			$scope.cameraFlash = result.flash;
			$scope.cameraISO = result.ISO;

			var maxNum = 5,
				minNum = 1; 
				n = Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum; 
			var filter_fNumber = {
				catId: parseInt($scope.catId),
				fNumber_clean: $scope.cameraAperture
			};
			var apertureComparator = function (a, b) {
				if (a.exposureTime_clean < b.exposureTime_clean) return -1;
				if (a.exposureTime_clean > b.exposureTime_clean) return 1;
				return 0;
			};
			Parse.getClass('PhotoObject', filter_fNumber, 9, n).then(function(data) {
				$scope.sameAperture = data.results.sort(apertureComparator);
				console.log(data);
			});

			var filter_exposureTime = {
				catId: parseInt($scope.catId),
				exposureTime: $scope.cameraShutter
			};
			n = Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum; 
			var fNumberComparator = function (a, b) {
				if (a.fNumber_clean < b.fNumber_clean) return -1;
				if (a.fNumber_clean > b.fNumber_clean) return 1;
				return 0;
			};
			Parse.getClass('PhotoObject', filter_exposureTime, 9, n).then(function(data) {
				$scope.sameExposureTimePhotos = data.results.sort(fNumberComparator);
				console.log(data.results.sort(fNumberComparator));
			});
		});
		

	}]);
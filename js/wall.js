angular.module('wall', 
	[
		'wu.masonry',
		'infinite-scroll'
	])
	.factory('WallPhotos', function($http, Parse) {
		var WallPhotos = function(filter) {
			this.photos = [];
			this.busy = false;
			this.after = '';
			this.perPage = 20;
			this.page = 1;
			this.filter = filter;
		};

		WallPhotos.prototype.nextPage = function() {
			if (this.busy) return;
			this.busy = true;

			Parse.getClass('PhotoObject', this.filter, this.perPage, this.page)
			.then(function(data) {

				function getSizeM(photo) {
					photo.squareSource = photo.squareSource.replace('_s.jpg', '_n.jpg');
					return photo;  
				}

				var photos = data.results.map(getSizeM);
// console.log(photos);
				for (var i = 0; i < photos.length; i++) {
					this.photos.push(photos[i]);
				}
				this.page = this.page + 1;
				this.busy = false;
			}.bind(this));

		};

		return WallPhotos;
	})
	.controller('wallCtrl', function($scope, $rootScope, $routeParams, Parse, WallPhotos) {
		var getIndex = function(objectId, array) {
			var i=0;
			for(i=0; i<array.length; i++) {
				if(array[i].objectId == objectId) {
					break;
				}
			}
			return i;
		}
		var categoryId = $routeParams.categoryId;
		console.log(categoryId);
		$rootScope.catId = categoryId;

		var filter = {
			catId: parseInt($routeParams.categoryId)
		};
		$scope.wallPhotos = new WallPhotos(filter);

		$scope.$on('changeCategoryId', function(event, categoryId) {
			// console.log('change' + categoryId);
			var filter = {
				catId: parseInt(categoryId)
			};
			$scope.wallPhotos = null;
			$scope.wallPhotos = new WallPhotos(filter);
			$scope.wallPhotos.nextPage();
		});
		// Parse.getClass('PhotoObject', filter)
		// .then(function(data) {
		// 	console.log(data);

		// 	// $scope.originData = data.results;
		// 	function getSizeM(photo) {
		// 		photo.squareSource = photo.squareSource.replace('_s.jpg', '_m.jpg');
		// 		return photo;  
		// 	}

		// 	var photos = data.results.map(getSizeM);
		// 	// console.log(photos);
		// 	$scope.photos = photos;
		// }, function(reason) {
		// 	console.log(reason);
		// });

		$scope.delPhoto = function(objectId) {
			console.log('del '+objectId);
			Parse.deleteObject('PhotoObject', objectId)
			.then(function(data) {
				console.log(data);
				$scope.wallPhotos.photos.splice(getIndex(objectId, $scope.wallPhotos.photos), 1);
			}, function(reason) {
				console.log(reason);
			});
		};
	});
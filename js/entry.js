angular.module('entry', 
	[
		'utilities'
	])
	.directive('circleMenu', function($rootScope, $compile, Flickr, $location) {
		var ctrller = function($scope, $element) {
				$scope.exploreHide = true;
				$scope.cateId;
				$scope.selectType = function(event, index, catId) {
					$scope.exploreHide = false;
					$scope.cateId = catId;
					var menuElem = $element.find('.menu')[0],
						rotateValArr = [72, 38, 0, -35, -73, 252, 217, 180, 143, 108],
						photoArr = [];
						// photoIdArr = ['8027055998', '8660915575', '9717018570'];
					menuElem.style.MozTransform = menuElem.style.webkitTransform = 'rotate('+rotateValArr[index]+'deg)';
					// Flickr.photosGetSizes('flickr.photos.getSizes', '8027055998').then(function(data) {
					// 	console.log(data[8]);
					// });
					console.log('get:' + catId);
					$rootScope.catId = catId;
					switch(index) {
						case 0:
						case 5:
							photoArr = ['night/3915331736_8a17cf5d63_o.jpg', 'night/4390009344_b40222a273_b.jpg', 'night/10616415036_9aee40b353_h.jpg', 'night/shutterstock_Boston_40347166.jpg'];
							break;
						case 1:
						case 6:
							photoArr = ['people/8031165513_1533639fec_h.jpg', 'people/9194707040_95b5f87f59_h.jpg', 'people/9693520408_c8a6d51e6e_h.jpg', 'people/MG_90042012-09-20-16-00-56.jpg'];
							break;
						case 2:
						case 7:
							photoArr = ['landscape/9717018570_312df6de89_h.jpg', 'landscape/10613730766_70feda83de_h.jpg', 'landscape/10616013274_c388bfd980_h.jpg', 'landscape/10624607543_7a65998786_h.jpg'];
							break;
					}

					var path = $location.path().match(/wall/gi);
					if(path == null){
						for(var i=0, len=photoArr.length; i<len; i++) {
							$element.parent().find('.slideshow li span')[i].style.backgroundImage = 'url(flickr/'+photoArr[i]+')';
						}
					}else{
						// $location.path('/wall/'+$scope.catId);
						$scope.$emit('changeCategoryId', $scope.catId);
					}
				};
				$scope.gotoWall = function() {
					$location.path('/wall/' + $scope.catId);
				};
			},
			linker = function(scope, element, attrs) {
				//
			};
		return {
			restrict: 'E',
            replace: false,
            transclude: false,
            templateUrl: 'views/circlemenu.html',
            controller: ctrller,
            link: linker
		}
	})
	.controller('entryCtrl', function($scope, $rootScope) {
		//
	});
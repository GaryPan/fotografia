angular.module('utilities', 
	[
	])
	.provider('Flickr', function() {
		var apiKey = "";
		this.getUrl = function(method, photoId) {
			return 'http://api.flickr.com/services/rest/?method=' + method + '&photo_id=' + photoId + '&format=json&api_key='+this.apiKey+'&nojsoncallback=1';
		};
		this.setApiKey = function(key) {
			if(key) this.apiKey = key;
		};
		this.$get = function($q, $http) {
			var self = this;
			return {
				photosGetSizes: function(method, photoId) {
					var d = $q.defer();
					$http({
						method: 'GET',
						url: self.getUrl(method, photoId),
						cache: true
					}).success(function(data) {
						d.resolve(data.sizes.size);
					}).error(function(err) {
						d.reject(err);
					});
					return d.promise;
				},
				photosGetInfo: function(method, photoId) {
					var d = $q.defer();
					$http({
						method: 'GET',
						url: self.getUrl(method, photoId),
						cache: true
					}).success(function(data) {
						d.resolve(data);
					}).error(function(err) {
						d.reject(err);
					});
					return d.promise;
				}
			}
		};
	})
	.factory('Parse', ['$http', '$q', function($http, $q) {
		var header = {
			'X-Parse-Application-Id': 'l8YIQiM8vKxOqGcOyTqcAYBkGPIWvTKnX9KevvOi',
			'X-Parse-REST-API-Key': 'i22UtKYOfDEOEUkyWOkxufBmaTcutlll3xMU7qZ5'
		};
		
		return {
			getClass: function(className, filter, perPage, page) {
				var deferred = $q.defer();
				if(!perPage)	perPage=30;
				if(!page)	page=1;
				var skip = (page==1) ? 0 : (page-1)*perPage;
				$http({
					method: 'GET',
					url: 'https://api.parse.com/1/classes/' + className + '?limit=' + perPage + '&skip=' + skip + '&where=' + JSON.stringify(filter),
					headers: header
				}).success(function(data, status, headers, config) {
					deferred.resolve(data);
				}).error(function(data, status, headers, config) {
					deferred.reject(data);
				});;
				return deferred.promise;
			},
			deleteObject: function(className, objectId) {
				var deferred = $q.defer();
				$http({
					method: 'DELETE',
					url: 'https://api.parse.com/1/classes/' + className + '/' + objectId,
					headers: header
				}).success(function(data, status, headers, config) {
					deferred.resolve(data);
				}).error(function(data, status, headers, config) {
					deferred.reject(data);
				});;
				return deferred.promise;
			}
		}
	}]);
